#include "Date.h"
#include "catch.hpp"
#include <string>
#include <sstream>
#include <stdexcept>

Date::Date() : Date{1970, 1, 1}
	{
	  
	}
Date::Date(int y, int m, int d) : year{y}, month{m}, day{d}
	{
	  throwExceptions(y, m, d);
	}

void Date::throwExceptions(int y, int m, int d){
  if(m < 0 || m > 12)
    {
      throw std::range_error("Wrong month");
    }
  if(d<1)
    {
      throw std::range_error("Wrong day");
    }
  if(d>month_days[m-1])
    {
      if((m=2 && isLeapYear(y) && d<=29))
	{
	}else
	{
	  throw std::range_error("Wrong day");
	}
    }
}
Date::Date(std::string date)
{
  std::stringstream ss;
  int year,month,day;
  ss << date;
  ss >> year;
  ss >> month;
  ss >> day;
  if(month <0)
    {
      month*=-1;
    }
  if(day <0)
    {
      day*=-1;
    }
  this->year = year;
  this->month = month;
  this->day = day;
  throwExceptions(year, month, day);
}


Date& Date::next_Date()
{
  day++;
  if(day > month_days[month-1])
    {
      if(month==2 && isLeapYear(year) && day ==29)
	{
	}else
	{
	  day = 1;
	  month++;
	}
    }
  if(month > 12)
    {
      month = 1;
      year++;
    }
  return *this;
}

Date& Date::previous_Date(){
  day--;
  if(day <1)
    {
      month--;
      if(month < 1)
	{
	  month = 12;
	  year--;
	}
      if(isLeapYear(year) && month ==2)
	{
	  day = 29;
	}else
	{
	  day = month_days[month-1];
	}
    }
  return *this;
}


bool Date::isLeapYear(int year)
{
  return (year%4 ==0 && year%100!=0) || (year%400==0);
}


std::string Date::to_string() const
{
  std::stringstream ss;
  ss << get_year() << "-" << std::setw(2) << std::setfill('0')
     << get_month() << "-" << std::setw(2) << std::setfill('0') << get_day();
  std::string s;
  ss >> s;
  return s;
}


int Date::get_year() const
{
  return year;
}
int Date::get_month() const
{
  return month;
}
int Date::get_day() const
{
  return day;
}

std::string string(Date d)
{
  return d.to_string();
}
	

std::stringstream& operator<<(std::stringstream & lhs, const Date & rhs)
{
  lhs << rhs.to_string();
  return lhs;
}

Date operator++(Date &d,int)
{ //d++
  Date temp = d;
  d.next_Date();
  return temp;
}

Date operator--(Date &d,int)
{ //d--
  Date temp = d;
  d.previous_Date();
  return temp;
}

Date& operator++(Date &d)
{ //++d
  return d.next_Date();
}

Date& operator--(Date &d)
{ //--d
  return d.previous_Date();
}

Date operator+(const Date lhs, const int rhs)
{
  Date temp = lhs;
  for(int i=0; i<rhs; i++)
    {
      temp.next_Date();
    }
  return temp;
}
Date operator-(const Date lhs, const int rhs)
{
  Date temp = lhs;
  for(int i=0; i<rhs; i++)
    {
      temp.previous_Date();
    }
  return temp;
}

Date& operator+=(Date & lhs, const int rhs) 
{
  lhs = lhs+rhs;
  return lhs;
}

Date& operator-=(Date & lhs, const int rhs) 
{
  lhs = lhs-rhs;
  return lhs;
}



bool operator==(const Date& lhs, const Date& rhs)
{

  return (lhs.get_year() == rhs.get_year() && lhs.get_month() == rhs.get_month()
	  && lhs.get_day() == rhs.get_day());
}

bool operator!=(const Date& lhs, const Date& rhs) 
{
  return !(lhs==rhs);
}

bool operator>(const Date& lhs, const Date& rhs) 
{
  if (lhs.get_year() > rhs.get_year()) 
    {
      return true;
    } else if (lhs.get_year() == rhs.get_year() ) 
    {
      if (lhs.get_month() > rhs.get_month()) 
	{
	  return true;
	} else if (lhs.get_month() == rhs.get_month())
	{
	  if (lhs.get_day() > rhs.get_day())
	    {
	      return true;
	    }
	}
    }
  return false;
}


bool operator<(const Date& lhs, const Date& rhs)
{

  if(lhs!=rhs && lhs > rhs)
    { 
      return false;
    }
  return true;
} 


TEST_CASE("Default constructor")
{
  Date d{};

  CHECK(d.get_year()  == 1970);
  CHECK(d.get_month() ==    1);
  CHECK(d.get_day()   ==    1);
}

TEST_CASE("String constructor")
{
  Date d("1950-5-04");

  CHECK(d.get_year()  == 1950);
  CHECK(d.get_month() ==    5);
  CHECK(d.get_day()   ==    4);

}
// This row makes the compiler skip the rest. Move it one test case at the time...
//#if 0
TEST_CASE("Constructor with numeric arguments")
{
  Date d{1904,2,29};

  CHECK(d.get_year()  == 1904);
  CHECK(d.get_month() ==    2);
  CHECK(d.get_day()   ==   29);
}

TEST_CASE("Constructor with wrong month and/or day")
{
  CHECK_THROWS(( Date{2000,  2,  39} ));
  CHECK_THROWS(( Date{2000, 32,  22} ));
  CHECK_THROWS(( Date{2000, -2, 129} ));
  CHECK_THROWS(( Date{2000, -2,  12} ));
  CHECK_THROWS(( Date{2000,  2, -12} ));
}

TEST_CASE("Leap year in constructor")
{
  CHECK_NOTHROW(( Date{2004, 2, 29} ));
  CHECK_NOTHROW(( Date{2000, 2, 29} ));
  CHECK_THROWS (( Date{1900, 2, 29} ));
}

TEST_CASE ("Convert to string")
{
  CHECK( (Date{ 2012,  3, 12}.to_string()) ==  "2012-03-12");
  CHECK( (Date{ 2012,  3,  2}.to_string()) ==  "2012-03-02");
  CHECK( (Date{ 2012, 11, 12}.to_string()) ==  "2012-11-12");
  CHECK( (Date{  201,  3,  2}.to_string()) ==   "201-03-02");
  CHECK( (Date{20121,  3,  2}.to_string()) == "20121-03-02");
}

TEST_CASE("Conversion to string")
{
  CHECK( string(Date{ 2012,  3, 12}) ==  "2012-03-12");
  CHECK( string(Date{ 2012,  3,  2}) ==  "2012-03-02");
  CHECK( string(Date{ 2012, 11, 12}) ==  "2012-11-12");
  CHECK( string(Date{  201,  3,  2}) ==   "201-03-02");
  CHECK( string(Date{20121,  3,  2}) == "20121-03-02");
}

TEST_CASE("Output operator")
{
  std::stringstream ss;
  SECTION("Simple output")
    {
      ss << Date{2012, 3, 12};
      CHECK(ss.str() == "2012-03-12");
    }
  SECTION("Chained output")
    {
      ss << Date{2012, 3, 12} << "---";
      CHECK(ss.str() == "2012-03-12---");
    }
  SECTION("Const date")
    {
      const Date d{2012, 12, 12};
      ss << d;
      CHECK(ss.str() == "2012-12-12");
    }
}

// Bra test!
TEST_CASE("OPERATOR d++"){
  Date d(1985,3,5);
  CHECK(d++  ==   Date("1985-03-05"));
  CHECK(d.to_string()  ==   "1985-03-06");
  d = Date(2004,2,28);
  d++;
  CHECK(d.to_string()  ==   "2004-02-29");

  d = Date(2015,12,31);
  d++;
  CHECK(d.to_string()  ==   "2016-01-01");
}


TEST_CASE("OPERATOR d--"){
  Date d(1985,3,5);
  CHECK(d--  ==   Date("1985-03-05"));
  CHECK(d.to_string()  ==   "1985-03-04");
  d = Date(1993,1,1);
  d--;
  CHECK(d.to_string()  ==   "1992-12-31");

  d = Date(2004,3,1);
  d--;
  CHECK(d.to_string()  ==   "2004-02-29");
}

TEST_CASE("OPERATOR ++d"){
  Date d(1985,3,5);
  CHECK(++d  ==   Date("1985-03-06"));
  CHECK(d  ==   Date("1985-03-06"));
  d = Date(2004,2,28);
  CHECK(++d  ==   Date("2004-02-29"));
  CHECK(d  ==   Date("2004-02-29"));
  d = Date(2015,12,31);
	  
  CHECK(++d  ==   Date("2016-01-01"));
  CHECK(d  ==   Date("2016-01-01"));
}



TEST_CASE("OPERATOR --d"){
  Date d(1985,3,5);
  CHECK(--d  ==   Date("1985-03-04"));
	  
  d = Date(2004,2,28);
  CHECK(--d  ==   Date("2004-02-27"));

  d = Date(2015,12,31);
	  
  CHECK(--d  ==   Date("2015-12-30"));
}

TEST_CASE("OPERATOR +"){
  Date d(1985,3,5);
  CHECK((d + 500).to_string()  ==   "1986-07-18");
  CHECK(d.to_string()  ==   "1985-03-05");
}

TEST_CASE("OPERATOR -"){
  Date d(1985,3,5);
  CHECK((d - 549).to_string()  ==   "1983-09-03");
  CHECK(d.to_string()  ==   "1985-03-05");
}

TEST_CASE("OPERATOR +="){
  Date d(1985,1,1);
  d+=5;
  CHECK(d.to_string()  ==   "1985-01-06");
}

TEST_CASE("OPERATOR -="){
  Date d(1985,5,15);
  d-=5;
  CHECK(d.to_string()  ==   "1985-05-10");
}

TEST_CASE("OPERATOR =="){
  Date a(2017,5,9);
  Date b(2017,5,9);
	  
  CHECK(a  ==  b);
  b = Date(2015,06,01);
  CHECK_FALSE(a  ==  b);
}

TEST_CASE("OPERATOR !="){
  Date a(2017,5,9);
  Date b(2017,5,9);
  CHECK(!(a  !=  b));
  b = Date(2015,06,01);
  CHECK(a  !=  b);
}

TEST_CASE("OPERATOR >"){
  Date a(2017,5,10);
  Date b(2017,5,9);
  CHECK(a  >  b);
}

TEST_CASE("OPERATOR <"){
  Date a(2017,5,10);
  Date b(2017,5,9);
	 
  CHECK(b < a);
} 



