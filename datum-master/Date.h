#include <string>

class Date{
 public:
  
  Date();
  Date(int y, int m, int d);
  explicit Date(std::string date); // 5-4
  std::string to_string() const;
  bool isLeapYear(int year);
  int get_year() const;
  int get_month() const;
  int get_day() const;
  Date& next_Date();
  Date& previous_Date();
 private:
  int month_days[12] ={31,28,31,30,31,30,31,31,30,31,30,31}; // Snygg lösning
  void throwExceptions(int year, int month, int day);
  int year;
  int month;
  int day;
};
bool operator<(const Date& lhs, const Date& rhs) ;
bool operator>(const Date& lhs, const Date& rhs);
std::stringstream& operator<<(std::stringstream & lhs, const Date & rhs);
Date operator++(Date &d,int);
Date operator--(Date &d,int);
Date& operator++(Date &d);
Date& operator--(Date &d);
Date operator+(Date const lhs, const int rhs);
Date operator-(Date const lhs, const int rhs);
Date& operator+=(Date & lhs, const int rhs) ;
Date& operator-=(Date & lhs, const int rhs);
bool operator==(const Date& lhs, const Date& rhs);
bool operator!=(const Date& lhs, const Date& rhs);



